package punkt8;

public class Main {

	public static void main(String[] args) {

		show(9);
		show(9.22);
		show("Abc");
		
	}

	public static void show(int myInt) {
		System.out.println("int");
		System.out.println(myInt);
	}
	
	public static void show(double myDouble) {
		System.out.println("double");
		System.out.println(myDouble);
	}
	
	public static void show(String myString) {
		System.out.println("string");
		System.out.println(myString);
	}
	
}
