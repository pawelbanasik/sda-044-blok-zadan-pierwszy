package punkt1i2;

public class Main {

	public static void main(String[] args) {

		// Zadanie 1a
		int[] myArray = { 1, 2, 3, 4, 5, 6, 7, 8 };
		// int suma = 0;
		//
		// for (int i = 0; i < myArray.length; i++) {
		//
		// suma += myArray[i];
		//
		// }
		// System.out.println(suma); // wynik 36

		// Zadanie 1b
		// int iloczyn = 1;
		// for (int i =0; i < myArray.length; i++) {
		//
		// iloczyn *= myArray[i];
		//
		// }
		// System.out.println(iloczyn);

		// Zadanie 1c

		// int a = 5;
		// int silnia = 1;
		//
		//
		// for (int i = 1; i <= a; i++ ) {
		//
		// silnia *= i;
		//
		// }
		// System.out.println(silnia);

		// Zadanie 1d
		// int a = 5;
		// int b = 2*a+3;
		//
		// for (int i=1; i < a; i++) {
		//
		// b += 2*i+3;
		// }
		// System.out.println(b);

		// // Zadanie 1g
		// String input1 = "Ala ma kota";
		// char[] inputSentence = input1.toLowerCase().toCharArray();
		//
		// int count = 0;
		//
		// String input2 = "A";
		// char[] inputLetter = input2.toLowerCase().toCharArray();
		//
		//
		// for (int i = 0; i < inputSentence.length; i++) {
		//
		// if (inputLetter[0] == inputSentence[i]) {
		// count++;
		// }
		//
		// }
		// System.out.println(count);

		// Zadanie 2a
		// int suma = 0;
		//
		// for (int element: myArray) {
		// suma += element;
		//
		// }
		// System.out.println(suma); // wynik 36

		// Zadanie 2b
		// int iloczyn = 1;
		//
		// for (int element: myArray) {
		//
		// iloczyn *= element;
		//
		// }
		// System.out.println(iloczyn);

		// Zadanie 2c

		// int a = 5;
		//
		// int[] silniaArray = new int[a];
		//
		// for (int i = 0; i < a; i++) {
		//
		// silniaArray[i] = i + 1;
		//
		// }
		// int silnia = 1;
		// for (int element: silniaArray) {
		//
		// silnia *= element;
		// }
		//
		// System.out.println(silnia); // wynik 120 mozna to w metode wkopiowac
		// bedzie to samo tylko jako parametr dajemy a

		// zliczanie("Ala ma kota", "A");
	}

	// // Zadanie 2d
	// public static void zliczanie(String input, String znak) {
	//
	// char[] inputSentence = input.toLowerCase().toCharArray();
	// char[] inputLetter = znak.toLowerCase().toCharArray();
	//
	// int count = 0;
	// for (char i : inputSentence) {
	// if (i == inputLetter[0]) {
	// count++;
	// }
	// }
	//
	// System.out.println(count);
	// }
}
